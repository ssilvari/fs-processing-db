#!/bin/bash
#
# Submission script for the helloWorld program
#
#OAR -l /nodes=1/core=50,walltime=96:00:00
#OAR -p cputype='xeon'
#
# The job is submitted to the default queue
#OAR -q default
# 
# Path to the binary to run

python3 recon_all_ukbb.py
