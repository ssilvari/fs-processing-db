# FreeSurfer recon-all on ukbiobank
import os
import argparse
from zipfile import ZipFile
from os.path import join, dirname, basename, realpath, isdir

from multiprocessing import Pool

import pandas as pd

# Some importan paths
current_dir = dirname(realpath(__file__))


def mkdir(folders):
    for f in folders:
        try:
            os.mkdir(f)
        except OSError:
            pass


def parse_args():
    sd_default = '/home/ssilvari/Documents/data/ukbiobank-freesurfer'
    dd_default = '/data/epione/share/private/ukbiobank-brains-2057/imaging/'

    parser = argparse.ArgumentParser(description='Process MIRIAD database.')
    parser.add_argument('-sd', metavar='--subjects-dir',
                        help='$SUBJECTS_DIR path.',
                        default=sd_default)
    parser.add_argument('-dd', metavar='--dataset-dir',
                        help='path to MIRIAD dataset.',
                        default=dd_default)
    return parser.parse_args()


def get_images(folder, ext='zip', img_code='20252'):
    # Look for file extension
    target_files = []

    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.endswith(ext) and img_code in file:
                filename = os.path.join(root, file)
                # print(filename)  # DEBUG
                target_files.append(filename)
    return target_files


def recon_all(zipped_subject):
    subject = zipped_subject[-21:-4]
    subject_name = subject[-27:-4]
        
    # Define tmp folder and final folder for each subject
    subject_tmp_folder = join(subjects_tmp, subject_name)
    subject_fin_folder = join(subjects_dir, subject_name)

    if not isdir(subject_fin_folder):
        # Extract T1 file to be processed 'T1_brain.nii.gz'
        zf = ZipFile(zipped_subject)
        zf_content = zf.namelist()
        t1_file = 'T1/T1_brain.nii.gz'
        tmp_folder = '/tmp/%s' % subject

        if any(t1_file in f for f in zf_content):
            print('Processing subject: %s' % subject)
            zf.extract(t1_file, tmp_folder)

            # process subject
            subject_file = join(tmp_folder, t1_file)

            print('File: ' + subject_file)
            command = 'recon-all -i ' + subject_file + \
                    ' -sd ' + subjects_tmp + \
                    ' -s ' + subject_name + \
                    ' -all'
            print(command)
            os.system(command)

            # Remove temporary file
            os.system('rm -rf %s' % tmp_folder)
            os.system('mv -v %s %s' % (subject_tmp_folder, subject_fin_folder))


if __name__ == '__main__':
    # Parse arguments
    os.system('clear')
    args = parse_args()

    dataset_dir = args.dd
    subjects_dir = args.sd
    subjects_tmp = '/dev/shm/ukbiobank_fs'
    
    # Create folders if they don't exist
    mkdir([subjects_dir, subjects_tmp])

    print('=== UKBIOBANK recon-all PREPROCESSING ===')
    print('\n\t- Database folder: %s' % dataset_dir)
    print('\n\t- FS output folder: %s' % subjects_dir)

    # Get all the set of images
    img_files = get_images(dataset_dir)

    # Bunches of 700
    # Doing: 0-700

    pool = Pool(45)
    pool.map(recon_all, img_files)
    pool.close()
    pool.join()
