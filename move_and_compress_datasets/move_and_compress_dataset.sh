#!/bin/bash
#OAR -l /nodes=1/core=5,walltime=96:00:00
#OAR -p cputype='xeon'
#
# The job is submitted to the default queue
#OAR -q default
# 

# Freesurfer processed dataset
DATASET_DIR="/home/ssilvari/Documents/data/ppmi_fs"
DESTINATION="/data/epione/user/ssilvari/freesurfer_datasets/ppmi_fs"

# Create dir if does not exist
mkdir ${DESTINATION}

# Get al the folders (subjects)
cd ${DATASET_DIR}
FOLDERS=(`ls -d */`)

for f in ${FOLDERS[@]};
do
# Remove "/" from name
SUBJECT_ID=${f%/}

CMD="zip -r "${DESTINATION}"/"${SUBJECT_ID}".zip "${DATASET_DIR}"/"${f}
echo ${CMD}
eval ${CMD}
done