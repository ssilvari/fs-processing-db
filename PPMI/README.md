# Run enigma shape for PPMI

Export the necessary environment variables:

- `export runDirectory=/home/ssilvari/Documents/code/meta-imagen/imaging/eshape/enigma_shape/`
- `export FS=/home/ssilvari/freesurfer`

Finally, run ENIGMA Shape pipeline

<code>/home/ssilvari/Documents/code/meta-imagen/imaging/eshape/enigma_shape/shape_group_run.sh \
/home/ssilvari/Documents/data/ppmi_fs/groupfile_beginner.csv \
/home/ssilvari/Documents/data/ppmi_fs/ \
/dev/shm/ppmi_eshape/</code>