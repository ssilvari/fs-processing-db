import os
from os.path import join, dirname

import pandas as pd

os.system('clear')


if __name__ == '__main__':
    # Load common data CSV
    data_file = join(
        os.environ['HOME'],
        'Documents/temp/ppmi/metadata/Randomization_table.csv'
        )
    
    # Set groupfile and load it
    groupfile_csv = join(
        os.environ['HOME'],
        'Documents/temp/ppmi/groupfile.csv'
    )
    dfg = pd.read_csv(groupfile_csv, index_col=0)

    # Define variables to extract
    var_names = ['Sex', 'Age']

    # Load DataFrame: df
    df = pd.read_csv(data_file, index_col='PATNO')

    # Calculate age
    fmt = '%m/%Y'
    enroll_date = pd.to_datetime(df.loc[:, 'ENROLLDT'], format=fmt)
    birth_date = pd.to_datetime(df.loc[:, 'BIRTHDT'], format=fmt)

    # Extract and preprocess sex and age
    sex = df['GENDER'] - 1
    age = pd.to_numeric((enroll_date - birth_date)) * 1e-9 / (60 * 60 * 24 * 365.25)
    
    # Common data DataFrame: dfc
    dfc = pd.concat([sex, age], axis=1, ignore_index=False)
    dfc.columns = var_names
    
    # Reindex (get data from groupfile)
    dfc = dfc.reindex(dfg.index)
    
    # Save data
    print('Saving data. Shape: %s' % str(dfc.shape))
    dfc.to_csv(join(
        dirname(groupfile_csv),
        'common_data.csv'
    ))
