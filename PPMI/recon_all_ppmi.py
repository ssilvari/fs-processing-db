# Runs recon-all FreeSurfer pipeline over PPMI dataset
import os
from os.path import join, dirname, realpath, basename, isdir

from multiprocessing import Pool

import pandas as pd


# Some previous params
current_dir = dirname(realpath(__file__))


def get_images(folder, ext='nii'):
    # Look for file extension
    target_files = []

    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.endswith(ext):
                filename = os.path.join(root, file)
                target_files.append(filename)
    return target_files


def filter_list(list_to_filter, keywords):
    filtered_set = []
    for element in list_to_filter:
        if any(['/' + key + '/' in element for key in keywords]):
            filtered_set.append(element)
    return filtered_set


def recon_all(filename):
    subject = basename(filename).split('_')[1]
    hdd_subject_dir = join(hdd_dir, subject)
    
    if not isdir(hdd_subject_dir):
        command = 'recon-all -i ' + filename + \
                ' -sd ' + subjects_dir + \
                ' -s ' + subject + \
                ' -all'
        print(command)
        os.system(command)
        os.system('mv -v %s %s' % (join(subjects_dir, subject), hdd_subject_dir))


if __name__ == '__main__':
    # Data folder
    df_path = '/home/ssilvari/Documents/data/PPMI'
    subjects_dir = '/dev/shm/ppmi_fs'
    hdd_dir = '/home/ssilvari/Documents/data/ppmi_fs'

    os.mkdir(subjects_dir)

    # Load subjects (ONLY PD TAKEN)
    df = pd.read_csv(join(current_dir, 'baseline_dx.csv'))

    # Get all Nifti image in the way
    image_list = get_images(df_path)

    # Filter list:
    keywords = df['PATNO'].apply(str).values
    filtered_imlist = filter_list(image_list, keywords)
    
    print('Subjects to be processed: %d' % len(filtered_imlist))

    # Multicore processing
    pool = Pool(45)
    pool.map(recon_all, filtered_imlist)
    pool.close()
    pool.join()
    
