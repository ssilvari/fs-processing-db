#!/bin/env bash
JOB_ID=$1

eval "oarstat -j ${JOB_ID}"
eval "oarpeek ${JOB_ID} --tail && oarpeek ${JOB_ID} --err"
