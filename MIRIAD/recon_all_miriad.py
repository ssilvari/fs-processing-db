#!/bin/env python3
import os
from os.path import dirname, join, realpath, normpath, isfile, isdir
from dotenv import load_dotenv
from multiprocessing import Pool

import pandas as pd

# Load environment variables from the .env file
ROOT = dirname(realpath(__file__))
load_dotenv(dotenv_path=join(ROOT, '.env'))


def preprocess_miriad(df):
    """
    Preprocess MIRIAD CSV database. Creating the Folder paths following the file structure in the database.
    :param pd.DataFrame df: dataframe containing MIRIAD's data
    :return pd.DataFrame: DataFrame containing the additional columns of interest.
    """
    # Create folder following MIRIAD structure SID_DX_SEX
    df['Root Folder'] = df['Subject'] + '_' + df['Group'].str.replace('Control', 'HC') + '_' + df['M/F']

    # Create Visit variable in a standar way: 01, 02, ...
    df['Visit'] = df['MR ID'].apply(lambda x: f'{int(x.split("_")[2]):02d}')
    df['MRI'] = df['MR ID'].apply(lambda x: x.split("_")[-1])

    # Create second-level Folder and file name SID_DX_SEX_VISIT_MR_# e.g. miriad_188_AD_M_01_MR_1
    df['Inner Folder'] = df['Root Folder'] + '_' + df['Visit'] + '_MR_' + df['MRI']

    # Create filename path: Root folder + Inner Folder + Filename
    data_root = os.getenv('DATASET_FOLDER', '')
    df['Filename'] = data_root + '/' + df['Root Folder'] + '/' + df['Inner Folder'] + '/' + df['Inner Folder'] + '.nii'
    df['Filename'] = df['Filename'].apply(normpath)

    return df


def process_subject(subject_id):
    print('Processing subject:', subject_id)
    df = miriad_df.query(f'Subject == "{subject_id}" and Visit == "01"')
    output_folder = os.getenv("OUTPUT_FOLDER")
    subject_folder = join(output_folder, subject_id)

    baseline_scans = [nii_name for nii_name in df['Filename'].values]
    files_missed = [not isfile(fn) for fn in baseline_scans]

    if any(files_missed):
        raise FileNotFoundError(f'Files not found: {baseline_scans}')

    if baseline_scans and not isdir(subject_folder):
        in_files = ' '.join([f'-i {fname}' for fname in baseline_scans])
        cmd = f'recon-all {in_files} -sd /dev/shm/ -s {subject_id} -all'
        # cmd = f'mkdir /dev/shm/{subject_id}'
        print(cmd)
        os.system(cmd)

        # Move subjects from RAM to disk
        cmd = f'mv -v /dev/shm/{subject_id} {output_folder}'
        print(cmd)
        os.system(cmd)
    elif isdir(subject_folder):
        print(f'[ WARNING ] Folder {subject_folder} already exists.')


if __name__ == '__main__':
    # Load Database info
    miriad_df = pd.read_csv(join(ROOT, 'mr_sessions.csv'))
    miriad_df = preprocess_miriad(miriad_df)

    print(miriad_df.head())
    subjects = miriad_df['Subject'].unique()

    pool = Pool(int(os.getenv('N_CORES')))
    pool.map(process_subject, subjects)
    pool.close()
