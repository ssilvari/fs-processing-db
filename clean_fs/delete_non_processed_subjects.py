# This deletes those subjects that did not finished processing (surface reconstruction)

import os
from os.path import dirname as up


if __name__ == '__main__':
    # Data folder
    fs_folder = '/home/ssilvari/Documents/data/ppmi_fs'

    folder_list = [x[0] for x in os.walk(fs_folder)]
    subjects_to_delete = []

    for f in folder_list:
        if '/surf' in f:
            sf = os.listdir(f)
            rf = [
                'lh.white', 'rh.white', 
                'lh.sphere.reg', 'rh.sphere.reg',
                'lh.thickness', 'rh.thickness'
                ]
            conditions = [i not in sf for i in rf]

            if not sf or any(conditions):  # Empty dir
                subjects_to_delete.append(f)
    
    print('[  INFO  ] Subjects to be deleted: %d' % len(subjects_to_delete))
    for s in subjects_to_delete:
        subject_folder = up(s)
        print('[  INFO  ] Deleting subect: %s' % subject_folder)
        # os.system('rm -rf %s' % subject_folder)

