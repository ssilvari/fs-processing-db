import os
from os.path import join, dirname, realpath, basename

import pandas as pd

current_dir = dirname(realpath(__file__))


def mkdir(folders):
    for f in folders:
        try:
            os.mkdir(f)
        except OSError:
            pass


if __name__ == '__main__':
    # Set main folder, output_folder and groupfile.csv
    # data_folder = '/data/epione/user/ssilvari/'
    data_folder = join(os.environ['HOME'], 'Documents')

    fs_folder = join(data_folder, 'data/oasis_fs')
    tmp = join('/dev/shm/', basename(fs_folder) + '_eshape')

    # tmp = join('/tmp', basename(fs_folder) + '_eshape')
    eshape_folder = join(data_folder, 'eshape', basename(fs_folder) + '_eshape')
    groupfile_csv = join(fs_folder, 'groupfile.csv')

    # Set envrionment variables
    env ={
        'FS': os.environ['FREESURFER_HOME'],
        'runDirectory': join(os.environ['HOME'], 'Documents/code/metaimagen/imaging/eshape/enigma_shape/')
    } 


    # Clear and print info
    os.system('clear')
    print('========= FEATURE EXTRACTION =========')
    print('\t- FreeSurfer folder: %s' % fs_folder)
    print('\t- Temp folder: %s' % tmp)
    print('\t- Output Eshape folder: %s' % eshape_folder)
    print('\t- Groupfile: %s' % groupfile_csv)

    # Check folder or create them
    folders = [tmp, eshape_folder]  # TODO: add eshape_folder
    mkdir(folders)

    # Load dataset
    chunks = pd.read_csv(groupfile_csv, index_col=0, chunksize=107)
    
    for i, df in enumerate(chunks):
        c = i + 1
        chunk_folder = join(tmp, 'chunk_%d' % c)
        mkdir([chunk_folder])

        chunk_csv = join(chunk_folder, 'groupfile_%s.csv' % c)
        df.to_csv(chunk_csv)

        set_env = ''.join(['export %s=%s && ' % (key, val) for key,val in env.items()])
        print(set_env)
        cmd = 'bash ' + join(env['runDirectory'], 'shape_group_run.sh') + ' ' + \
              chunk_csv + ' ' + \
              fs_folder + ' ' + \
              chunk_folder
        
        # Join commands
        cmd = set_env + cmd
        
        # Run as daemon by using tmux
        sess_name = 'chunk_%d' % (i + 1)
        daemon = 'tmux new-session -d -s %s "%s"' % (sess_name, cmd)

        print(' -- RUNNING CHUNK %d --' % (i + 1))
        print(daemon)
        os.system(daemon)
    
    # os.system('mv -v %s %s' % (tmp, eshape_folder))