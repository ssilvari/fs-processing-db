import os
from os.path import join

import pandas as pd


if __name__ == '__main__':
    # Define main folder and number of chunks
    data_folder = '/home/ssilvari/Documents/eshape/ppmi_fs_eshape'
    chunks = 47

    # Thickness and jacobian Dataframe lists: tdfs, jdfs
    tdfs = []
    jdfs = []

    # Load merge and save
    for c in range(1, chunks + 1):
        # Chunk folder: cf
        cf = join(data_folder, 'chunk_%d' % c)
        print('[  INFO  ] Loading: %s' % cf)

        # Thickness and jacobians file: tf, jf
        tf = join(cf, 'groupfile_%d_thick.csv' % c)
        jf = join(cf, 'groupfile_%d_LogJacs.csv' % c)

        # Read files as dataframes: tdf, jdf
        tdf = pd.read_csv(tf, index_col=0)
        jdf = pd.read_csv(jf, index_col=0)

        # Append to dataframe lists
        tdfs.append(tdf)
        jdfs.append(jdf)
    
    # Join and save global
    thick_df = pd.concat(tdfs)
    jacob_df = pd.concat(jdfs)

    print('[  INFO  ] Saving final data')
    thick_df.to_csv(join(data_folder, 'groupfile_thick.csv'))
    jacob_df.to_csv(join(data_folder, 'groupfile_LogJacs.csv'))

    print('[  OK  ] DONE!')
