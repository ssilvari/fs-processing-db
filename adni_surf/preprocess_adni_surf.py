# Reorganize surfaces obtained from ADNI (folder structure)
import os
from os.path import dirname as up


def get_surf_folders(folder):
    target_folders = []
    for root, dirs, files in os.walk(folder):
        if root.endswith('/surf'):
            target_folders.append(root)
    return target_folders


if __name__ == '__main__':
    data_folder = '/user/ssilvari/home/Documents/temp/ADNI_SURF'
    folders = get_surf_folders(data_folder)
    
    # Clear screen and move surf data
    os.system('clear')
    print('Surf folders: %d' % len(folders))
    
    subjects = next(os.walk(data_folder))[1]  # [x[0] for x in os.walk(data_folder)]
    print('Folders in root: %d' % len(subjects))
    

    # for folder in folders:
    #     subject_folder = up(up(up(up(folder))))
    #     folder_to_delete = up(up(up(folder)))

    #     cmd = 'mv -v ' + folder + ' ' + subject_folder
    #     rmf = 'rm -rf ' + folder_to_delete
    #     print('Moving surf folder to: %s' % subject_folder)
    #     os.system(cmd)

    #     print('Deleting folder: %s\n' % folder_to_delete)
    #     os.system(rmf)