import os
import glob
from zipfile import ZipFile
from os.path import join, dirname


if __name__ == '__main__':
    # List of ZIP files: zip_list
    main_folder = '/run/media/ssilvari/Smith_2T_WD/Databases/ADNI_SURF/surfaces'
    out_f = '/home/ssilvari/Documents'
    zip_list = [join(main_folder, 'screening_surfaces_%d.zip' % i) for i in range(7,11)]
    ext = ['.white', '.sphere.reg', '.thickness']

    # Clear screen
    os.system('clear')
    print('========= ADNI SURFACES EXTRACTION =========\n')
    
    for zip_file in zip_list:
        print('[  INFO  ] Extracting %s...' % zip_file)
        # Extract files
        with ZipFile(zip_file, 'r') as zf:
            surf_files = []
            files_in_zip = zf.namelist()
            
            # Filter files per extension
            for fiz in files_in_zip:
                for e in ext:
                    if e in fiz:
                        surf_files.append(fiz)
            
            # Extract them
            for sf in surf_files:
                try:
                    zf.extract(sf, out_f)
                except Exception as e:
                    print('[  ERROR  ] Extracting file %s | Error detail:\n\t%s' % (sf, str(e)))
    
    # Print results
    surf_folders = []
    for root, dirs, files in os.walk(join(out_f, 'ADNI')):
        if 'surf' in dirs:
            surf_folders.append(os.path.join(root, 'surf'))
    
    for sf in surf_folders:
        destination = join( dirname(dirname(dirname(dirname(sf)))),
                            'surf' )
        os.rename(sf, destination)
    print("Done!")

