import os
from os.path import join, dirname

import pandas as pd


if __name__ == '__main__':
    # Load ADNIMERGE
    df = pd.read_csv(
            'https://raw.githubusercontent.com/sssilvar/CSD-AD/master/param/common/adnimerge.csv',
            index_col='PTID',
            low_memory=False)
    
    # Load groupfile.csv
    features_csv = '/home/ssilvari/Documents/eshape/adni_eshape/groupfile_thick.csv'
    subjects = pd.read_csv(features_csv, index_col=0).index

    # Get common data: Sex and Age
    # Set column names
    var_names = ['Sex', 'Age']

    # Filter data to baseline
    df = df[df['VISCODE'] == 'bl']

    cdf = df.reindex(subjects)
    cdf = cdf.loc[:, ['PTGENDER', 'AGE']]
    cdf['PTGENDER'] = cdf['PTGENDER'].apply(lambda x: int(x == 'Male'))
    cdf.columns = var_names

    cdf.to_csv(join(
        dirname(features_csv),
        'common_data.csv'
    ))